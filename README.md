[Download Duupa-1.0](https://) and find duplicate files.

This is a JavaFX interface that supports the features of [duupa-core](https://bitbucket.org/jeffbohmann/duupa-core).

To run Duupa, execute (double-click or run from the command prompt) duupa.sh or duupa.bat depending on your system.

The Duupa main window should appear. Click the help (?) button for further information about the using the application.