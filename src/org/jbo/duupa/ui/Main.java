package org.jbo.duupa.ui;

import javafx.application.Application;
import javafx.stage.Stage;
import org.jbo.duupa.ui.view.RootView;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        new Controller(new RootView(stage));
    }
}
