package org.jbo.duupa.ui;


import com.google.common.collect.Lists;
import com.sun.javafx.collections.ObservableListWrapper;
import com.sun.jna.platform.FileUtils;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import org.controlsfx.control.StatusBar;
import org.jbo.duupa.core.duupa.Duupa;
import org.jbo.duupa.core.duupa.RunOptions;
import org.jbo.duupa.core.hash.HashOptions;
import org.jbo.duupa.core.result.DuplicateResults;
import org.jbo.duupa.core.result.DuplicateResultsTools;
import org.jbo.duupa.ui.preview.Previewer;
import org.jbo.duupa.ui.view.*;
import org.jbo.duupa.ui.view.ToolBar;
import org.jbo.log.Log;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class Controller {

    //TODO: NOTE: call duplicateResults.removeSingles when a duplicate has been deleted, or indicate it is a single?

    public Controller(RootView rootView) {
        this.rootView = rootView;

        setInitialStates();
        setUpHandlers();
        setUpStatusBar();
        //TODO: remove this
        rootView.getToolBar().getDirectoryTextField().setText("/home/jeff");
    }

    private void setInitialStates() {
        this.rootView.requestFocus();
        this.rootView.getToolBar().getToggleSiblingsViewButton().setDisable(true);
    }

    private void setUpHandlers() {
        rootView.getToolBar().getSearchButton().setOnAction(this::onSearchButtonClicked);
        rootView.getToolBar().getDeleteButton().setOnAction(this::onDeleteButtonClicked);
        rootView.getToolBar().getToggleSiblingsViewButton().selectedProperty().addListener(this::onSiblingsViewToggleClicked);
        rootView.getToolBar().getDirectoryTextField().setOnKeyReleased(this::onDirectoryTextFieldKeyReleased);
        rootView.getToolBar().getDirectoryChooserButton().setOnAction(this::onDirectoryChooserButtonClicked);
        rootView.getResultsView().getFileList().getFocusModel().focusedItemProperty().addListener(this::onFileListFocuedItemChanged);
        rootView.getSiblingsView().getSiblingsList().getFocusModel().focusedItemProperty().addListener(this::onSiblingsListFocuedItemChanged);
    }

    private void onSiblingsViewToggleClicked(ObservableValue<?> observable, Boolean oldVal, Boolean newVal) {
        if(newVal) {
            showSiblingsView(
                    rootView.getResultsView().getFileList().getFocusModel().getFocusedItem().getValue());
        }
        else {
            showResultsView();
        }
    }

    private void onDirectoryTextFieldKeyReleased(KeyEvent keyEvent) {
        KeyCombination enter = new KeyCodeCombination(KeyCode.ENTER);
        if(enter.match(keyEvent)) {
            rootView.getToolBar().getSearchButton().fire();
        }
    }

    private void onSearchButtonClicked(ActionEvent event) {

        String searchMode = ToolBar.SEARCH_MODES.inverse().get(
                rootView.getToolBar().getSearchButton().getText());

        final Path searchPath = Paths.get(rootView.getToolBar().getDirectoryTextField().getText());
        new Duupa().start(
                searchPath, //TODO: quickSize user input, not 1024
                new RunOptions().hashOptions(new HashOptions().quick(searchMode.equals("QUICK")).quickSize(1024)),
                duplicateResults -> {
                    this.duplicateResults = duplicateResults;
                    loadResults();
                }
        );
    }

    private void onDeleteButtonClicked(ActionEvent event) {
        ResultsView resultsView = rootView.getResultsView();
        SiblingsView siblingsView = rootView.getSiblingsView();

        //TODO: only enable delete button if the current pane has selected items

        List<File> itemsToDelete = new ArrayList<>();
        if(rootView.getStackPane().getChildren().indexOf(resultsView) >
                rootView.getStackPane().getChildren().indexOf(siblingsView)) {
                    resultsView.getFileList().getCheckModel().getCheckedItems().forEach(pathTreeItem -> {
                        if(pathTreeItem.getChildren().isEmpty()) {
                            itemsToDelete.add(pathTreeItem.getValue().toFile());
                        }
                    });
        }
        else {
            itemsToDelete.addAll(siblingsView.getSiblingsList().getSelectionModel().getSelectedItems().stream()
                    .map(Path::toFile)
                    .collect(Collectors.toList()));
        }

        //double check with the user
        Alert deletePrompt = new Alert(Alert.AlertType.CONFIRMATION);
        deletePrompt.setHeaderText("Confirm Removal");
        deletePrompt.setContentText("This will remove " + itemsToDelete.size() + " item(s). Are you sure?");
        Optional<ButtonType> result = deletePrompt.showAndWait();
        if(result.get() != ButtonType.OK) {
            return;
        }

        String msg;
        if(FileUtils.getInstance().hasTrash()) {
            try {
                FileUtils.getInstance().moveToTrash(itemsToDelete.toArray(new File[0]));
                msg = "Moved " + itemsToDelete.size() + " item(s) to the trash.";
            }
            catch(IOException e) {
                String errorMessage = "Error moving file(s) to trash.";
                log.error(errorMessage, e);
                rootView.getStatusBar().setText(errorMessage);
                return;
            }
        }
        else {
            itemsToDelete.forEach(File::delete);
            msg = "Deleted " + itemsToDelete.size() + " item(s).";
        }

        log.info(msg);
        rootView.getStatusBar().setText(msg);
        log.info(itemsToDelete.stream()
                .map(File::toString)
                .collect(Collectors.joining("\n")));
    }

    private void onDirectoryChooserButtonClicked(ActionEvent event) {
        File searchLocation = rootView.getToolBar().getDirectoryChooserWindow().showDialog(rootView);
        if(searchLocation != null) {
            rootView.getToolBar().getDirectoryTextField().setText(searchLocation.getAbsolutePath());
        }
    }

    private void onFileListFocuedItemChanged(ObservableValue<? extends TreeItem<Path>> observable, TreeItem<Path> oldValue, TreeItem<Path> newValue) {

        //no focued item right now
        if(newValue == null) {
            preview(Paths.get(""));
            return;
        }

        //enable the siblings view button if there are siblings
        rootView.getToolBar().getToggleSiblingsViewButton().setDisable(
                new DuplicateResultsTools().getSiblings(duplicateResults, newValue.getValue()).isEmpty());

        //show the preview
        preview(newValue.getValue());
    }

    private void onSiblingsListFocuedItemChanged(ObservableValue<? extends Path> observable, Path oldValue, Path newValue) {
        rootView.getSiblingsView().getDuplicatesList().setItems(
                new ObservableListWrapper<>(Lists.newArrayList(siblingsWithDuplicates.get(newValue))));
    }

    private void setUpStatusBar() {
        StatusBar statusBar = rootView.getStatusBar();
        statusBar.setText("Ready");
    }

    private void loadResults() {
        TreeItem<Path> root = new TreeItem<>(Paths.get(""));

        duplicateResults.getAll().forEach(duplicateList -> {
            CheckBoxTreeItem<Path> grouper = new CheckBoxTreeItem<>(Paths.get(""));
            grouper.setExpanded(true);
            grouper.setIndeterminate(true);
            duplicateList.forEach(path -> grouper.getChildren().add(new CheckBoxTreeItem<>(path)));
            root.getChildren().add(grouper);
        });

        Platform.runLater(() -> rootView.getResultsView().getFileList().setRoot(root));
    }

    public void preview(Path path) {

        FilePreview filePreview = rootView.getResultsView().getFilePreview();
        VBox fileInfoPane = filePreview.getFileInfoPane();
        StackPane filePreviewPane = filePreview.getPreviewPane();

        fileInfoPane.getChildren().clear();
        filePreviewPane.getChildren().clear();

        if(path.toString().isEmpty()) {
            return;
        }

        //set up the preview area, registering a handler for the siblings view link
        Label fileNameLabel = new Label(path.toString());
        fileNameLabel.setWrapText(true);
        fileNameLabel.setFont(Font.font(Font.getDefault().getFamily(), FontWeight.BOLD, 14));
        fileInfoPane.getChildren().add(fileNameLabel);

        List<Path> siblings = new DuplicateResultsTools().getSiblings(duplicateResults, path);
        if(!siblings.isEmpty()) {
            Path parent = path.getParent();
            Hyperlink siblingLink = new Hyperlink(siblings.size() + " siblings (with duplicates) at " + parent);
            siblingLink.setWrapText(true);
            siblingLink.setOnAction(event -> rootView.getToolBar().getToggleSiblingsViewButton().setSelected(true));
            fileInfoPane.getChildren().add(siblingLink);
        }

        String fileName = path.toFile().getName();
        int dotIndex = fileName.lastIndexOf(".");
        if(dotIndex == -1) {
            displayUnknownFileType(path);
            return;
        }

        String extension = fileName.substring(dotIndex + 1).toLowerCase();
        for(Previewer previewer : filePreview.getPreviewers()) {
            if(previewer.supportedExtensions().contains(extension)) {
                Node node = previewer.render(path);
                filePreviewPane.getChildren().add(node);
                return;
            }
        }

        displayUnableToRenderPreview(path);
    }

    private void displayUnknownFileType(Path path) {
        rootView.getResultsView().getFilePreview().getPreviewPane().getChildren().add(
                new Label("Unknown file type: " + path.toFile().getName()));
    }

    private void displayUnableToRenderPreview(Path path) {
        rootView.getResultsView().getFilePreview().getPreviewPane().getChildren().add(
                new Label("Unable to preview: " + path.toFile().getName()));
    }

    private void showSiblingsView(Path selectedPath) {

        this.siblingsWithDuplicates = new DuplicateResultsTools().getChildrenWithDuplicates(duplicateResults, selectedPath.getParent());

        //take the keys from the map to use for the list of siblings
        rootView.getSiblingsView().getSiblingsList().setItems(new ObservableListWrapper<>(Lists.newArrayList(siblingsWithDuplicates.keySet())));
        rootView.getSiblingsView().getDuplicatesList().getItems().clear();
        rootView.getSiblingsView().toFront();
    }

    private void showResultsView() {
        //TODO: refresh the view (rerun scan?) or propagate deletes between views.
        rootView.getResultsView().toFront();
    }

    private DuplicateResults duplicateResults;
    private Map<Path, List<Path>> siblingsWithDuplicates;
    private RootView rootView;
    private static final Log log = Log.get();
}
