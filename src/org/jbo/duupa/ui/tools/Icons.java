package org.jbo.duupa.ui.tools;

import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;


public class Icons {

    public static Glyph get(FontAwesome.Glyph glyph) {
        return font.create(glyph);
    }

    private static GlyphFont font;
    static {
        //TODO: local font would be nice
        /**GlyphFontRegistry.register("awesome", Icons.class.getResourceAsStream(
                "/org/jbo/duupa/font/fontawesome-webfont.ttf"), 12);*/
        font = GlyphFontRegistry.font("FontAwesome");
    }
}
