package org.jbo.duupa.ui.tools;


import javafx.scene.image.Image;

public class Images {
    public static Image get(String fileName) {
        return new Image(Images.class.getResourceAsStream("/org/jbo/duupa/ui/img/" + fileName));
    }
}
