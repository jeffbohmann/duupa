package org.jbo.duupa.ui.view;


import com.google.common.collect.Sets;
import org.controlsfx.control.MasterDetailPane;
import org.jbo.duupa.ui.preview.ImagePreviewer;
import org.jbo.duupa.ui.preview.RawImagePreviewer;

public class ResultsView extends MasterDetailPane {
    public ResultsView() {
        setMasterNode(createFileDuplicateList());
        setDetailNode(createFilePreview());
        setDividerPosition(.5);
    }

    public ResultTreeList getFileList() {
        return fileList;
    }

    public FilePreview getFilePreview() {
        return filePreview;
    }

    private ResultTreeList createFileDuplicateList() {
        fileList = new ResultTreeList();
        return fileList;
    }

    private FilePreview createFilePreview() {
        filePreview = new FilePreview(Sets.newHashSet(
                new ImagePreviewer(),
                new RawImagePreviewer()
        ));
        return filePreview;
    }

    private ResultTreeList fileList;
    private FilePreview filePreview;
}
