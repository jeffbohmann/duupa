package org.jbo.duupa.ui.view;

import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.stage.DirectoryChooser;
import org.controlsfx.glyphfont.FontAwesome;
import org.jbo.duupa.ui.tools.Icons;


public class ToolBar extends javafx.scene.control.ToolBar {

    //TODO: improve this. enum?
    public static final BiMap<String,String> SEARCH_MODES =
            ImmutableBiMap.<String,String>builder()
            .put("FULL", "Full Search")
            .put("QUICK", "Quick Search")
            .build();

    public ToolBar() {
        Pane fill = new Pane();
        HBox.setHgrow(fill, Priority.ALWAYS);
        getItems().addAll(createLeftItems(), fill, createRightItems());
    }

    private Node createLeftItems() {
        return new HBox(
                createShowSiblingsViewButton(),
                createDeleteButton()
        );
    }

    private Node createShowSiblingsViewButton() {
        toggleSiblingsViewButton = new ToggleButton("Siblings View");
        return toggleSiblingsViewButton;
    }

    private Node createDeleteButton() {
        deleteButton = new Button("Delete");
        return deleteButton;
    }

    private Node createRightItems() {
        Separator separator = new Separator(Orientation.VERTICAL);
        separator.setPadding(new Insets(0, 5, 0, 5));
        return new HBox(
                createDirectoryChooserGroup(),
                separator,
                createSearchButton()
        );
    }

    public ToggleButton getToggleSiblingsViewButton() {
        return toggleSiblingsViewButton;
    }

    public Button getDeleteButton() {
        return deleteButton;
    }

    public SplitMenuButton getSearchButton() {
        return searchButton;
    }

    public TextField getDirectoryTextField() {
        return directoryTextField;
    }

    public Button getDirectoryChooserButton() {
        return directoryChooserButton;
    }

    public DirectoryChooser getDirectoryChooserWindow() {
        return directoryChooserWindow;
    }

    private Node createDirectoryChooserGroup() {

        //chooser
        directoryChooserWindow = new DirectoryChooser();
        directoryChooserWindow.setTitle("Duupa - Select a directory");

        //field
        //TODO: type ahead?
        directoryTextField = new TextField();
        directoryTextField.setPromptText("Where to search?");

        //file chooser button
        //TODO: make it work
        directoryChooserButton = new Button("", Icons.get(FontAwesome.Glyph.FOLDER_OPEN_ALT));

        return new HBox(directoryTextField, directoryChooserButton);
    }

    private Node createSearchButton() {
        String quickText = SEARCH_MODES.get("QUICK");
        String fullText = SEARCH_MODES.get("FULL");

        searchButton = new SplitMenuButton();
        searchButton.setGraphic(Icons.get(FontAwesome.Glyph.SEARCH));
        searchButton.setText(SEARCH_MODES.get("QUICK"));

        MenuItem quick = new MenuItem(quickText);
        quick.setOnAction(event -> searchButton.setText(quickText));
        MenuItem full = new MenuItem(fullText);
        full.setOnAction(event -> searchButton.setText(fullText));

        searchButton.getItems().addAll(
                quick,
                full);
        return searchButton;
    }

    private ToggleButton toggleSiblingsViewButton;
    private Button deleteButton;
    private TextField directoryTextField;
    private Button directoryChooserButton;
    private DirectoryChooser directoryChooserWindow;
    private SplitMenuButton searchButton;
}
