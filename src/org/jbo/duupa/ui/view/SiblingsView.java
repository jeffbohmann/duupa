package org.jbo.duupa.ui.view;

import javafx.geometry.Side;
import javafx.scene.Node;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import org.controlsfx.control.MasterDetailPane;

import java.nio.file.Path;


public class SiblingsView extends MasterDetailPane {

    public SiblingsView() {
        super(Side.RIGHT, true);
        setDividerPosition(.5);
        setMasterNode(createSiblingsList());
        setDetailNode(createDuplicatesList());
    }

    public ListView<Path> getSiblingsList() {
        return siblingsList;
    }

    public ListView<Path> getDuplicatesList() {
        return duplicatesList;
    }

    private Node createSiblingsList() {
        siblingsList = new ListView<>();
        siblingsList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        return siblingsList;
    }

    private Node createDuplicatesList() {
        duplicatesList = new ListView<>();
        return duplicatesList;
    }

    private ListView<Path> siblingsList;
    private ListView<Path> duplicatesList;
}
