package org.jbo.duupa.ui.view;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.jbo.duupa.ui.tools.Images;

public class RootView extends Stage {

    public RootView(Stage underlyingStage) {
        stage = underlyingStage;
        stage.setTitle("Duupa - find duplicate files");
        stage.getIcons().add(Images.get("duupa128.png"));
        stage.setScene(new Scene(createBorderPane(), 800, 600));
        stage.show();
    }

    public Stage getStage() {
        return this.stage;
    }

    public ToolBar getToolBar() {
        return toolBar;
    }

    public StackPane getStackPane() {
        return stackPane;
    }

    public ResultsView getResultsView() {
        return resultsView;
    }

    public SiblingsView getSiblingsView() {
        return siblingsView;
    }

    public StatusBar getStatusBar() {
        return statusBar;
    }

    private Pane createBorderPane() {
        BorderPane pane = new BorderPane();
        pane.setTop(createToolBar());
        pane.setCenter(createStackPane());
        pane.setBottom(createStatusBar());
        return pane;
    }

    private Node createStackPane() {
        stackPane = new StackPane(
                createSiblingView(),
                createResultTreeList()
        );
        return stackPane;
    }

    private Node createResultTreeList() {
        resultsView = new ResultsView();
        return resultsView;
    }

    private Node createSiblingView() {
        siblingsView = new SiblingsView();
        return siblingsView;
    }

    private Node createToolBar() {
        toolBar = new ToolBar();
        return toolBar;
    }

    private Node createStatusBar() {
        statusBar = new StatusBar();
        return statusBar;
    }

    private Stage stage;
    private ToolBar toolBar;
    private StackPane stackPane;
    private SiblingsView siblingsView;
    private ResultsView resultsView;
    private StatusBar statusBar;
}
