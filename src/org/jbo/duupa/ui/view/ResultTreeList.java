package org.jbo.duupa.ui.view;


import org.controlsfx.control.CheckTreeView;

import java.nio.file.Path;

public class ResultTreeList extends CheckTreeView<Path> {
    public ResultTreeList() {
        setShowRoot(false);
    }
}
