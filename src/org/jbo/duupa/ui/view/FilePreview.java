package org.jbo.duupa.ui.view;


import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import org.jbo.duupa.ui.preview.Previewer;

import java.util.Set;


public class FilePreview extends BorderPane {

    public FilePreview(Set<Previewer> previewers) {
        this.previewers = previewers;
        fileInfoPane = new VBox();
        previewPane = new StackPane();

        setTop(fileInfoPane);
        setCenter(previewPane);
    }

    public VBox getFileInfoPane() {
        return fileInfoPane;
    }

    public StackPane getPreviewPane() {
        return previewPane;
    }

    public Set<Previewer> getPreviewers() {
        return previewers;
    }

    private VBox fileInfoPane;
    private StackPane previewPane;
    private final Set<Previewer> previewers;
}
