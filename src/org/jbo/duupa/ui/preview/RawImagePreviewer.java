package org.jbo.duupa.ui.preview;


import com.google.common.collect.Lists;
import ij.ImagePlus;
import ij.io.Opener;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.awt.image.BufferedImage;
import java.nio.file.Path;
import java.util.List;

public class RawImagePreviewer implements Previewer {
    @Override
    public List<String> supportedExtensions() {
        return Lists.newArrayList("nef");
    }

    @Override
    public Node render(Path path) {
        ImagePlus imagePlus = new Opener().openImage(path.toString());
        BufferedImage bufferedImage = imagePlus.getBufferedImage();
        Image image = SwingFXUtils.toFXImage(bufferedImage, null);
        return new ImageView(image);
    }
}
