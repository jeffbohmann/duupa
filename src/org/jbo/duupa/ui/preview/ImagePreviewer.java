package org.jbo.duupa.ui.preview;


import com.google.common.collect.ImmutableList;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.jbo.log.Log;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class ImagePreviewer implements Previewer {
    @Override
    public List<String> supportedExtensions() {
        return ImmutableList.of("png", "jpg", "gif", "jps", "mpo");
    }

    @Override
    public Node render(Path path) {
        try {
            return new ImageView(new Image(Files.newInputStream(path, StandardOpenOption.READ)));
        }
        catch(IOException e) {
            log.error(e, "Failed to read file[{}]", path);
        }
        return null;
    }

    private static final Log log = Log.get();
}
