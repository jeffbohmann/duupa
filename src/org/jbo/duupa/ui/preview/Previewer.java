package org.jbo.duupa.ui.preview;


import javafx.scene.Node;

import java.nio.file.Path;
import java.util.List;

public interface Previewer {

    List<String> supportedExtensions();
    Node render(Path path);
}
